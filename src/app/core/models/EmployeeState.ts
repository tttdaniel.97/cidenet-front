import { Employee } from "./Employee";

export interface EmployeeState {
    employee: Employee;
}