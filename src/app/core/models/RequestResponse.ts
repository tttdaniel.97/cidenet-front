export interface RequestResponse {

    message: string;
    httpStatus: string;
    ZonedDateTime: Date;

}
