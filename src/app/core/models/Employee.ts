import { Area } from "./Area";

export interface Employee {

    idEmployee: string;
    fristSurname: string;
    secondSurname: string;
    name: string;
    otherNames: string;
    typeId: number;
    employmentsCountry: string;
    email: string;
    area: Area;
    status: string;
    registrationDate: Date;
    editionDate: Date;

}