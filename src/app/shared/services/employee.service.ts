import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Employee } from 'src/app/core/models/Employee';
import { RequestResponse } from 'src/app/core/models/RequestResponse';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private baseUrl: string = environment.url;

  constructor(private httpclient: HttpClient) { }

  getEmployees(): Observable<Employee[]> {
    return this.httpclient.get<Employee[]>(`${this.baseUrl}employees`);
  }

  createEmployee(employee: Employee): Observable<RequestResponse> {
    return this.httpclient.post<RequestResponse>(`${this.baseUrl}employee`, employee);
  }

  updateEmploye(employee: Employee): Observable<RequestResponse> {
    return this.httpclient.put<RequestResponse>(`${this.baseUrl}employee`, employee);
  }

  deleteEmployee(employee: Employee): Observable<RequestResponse> {
    return this.httpclient.delete<RequestResponse>(`${this.baseUrl}employee/${employee.idEmployee}`);
  }

}
