import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { map, Observable } from 'rxjs';
import { Employee } from 'src/app/core/models/Employee';
import { AppState } from 'src/app/state/app.state';
import { selectEmployee } from 'src/app/state/selectors/employee.selector';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ModalMessageComponent } from '../modals/modal-message/modal-message.component';
import * as moment from 'moment';
import { EmployeeService } from '../../services/employee.service';
import { Route, Router } from '@angular/router';
import { loadEmployeeUpdate } from 'src/app/state/actions/employee.actions';

export interface Validator {
  invalidName: boolean
}

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit {

  employee$: Observable<Employee>;
  idTypes = [
    {
      id: 1,
      description: 'Cédula de Ciudadanía'
    },
    {
      id: 2,
      description: 'Cédula de Extranjería'
    },
    {
      id: 3,
      description: 'Pasaporte'
    },
    {
      id: 4,
      description: 'Permiso Especial'
    }

  ];

  areas = [
    {
      id: 1,
      description: 'Administración'
    },
    {
      id: 2,
      description: 'Financiera'
    },
    {
      id: 3,
      description: 'Compras'
    },
    {
      id: 4,
      description: 'Infraestructura'
    },
    {
      id: 5,
      description: 'Operación'
    },
    {
      id: 6,
      description: 'Talento humano'
    },
    {
      id: 7,
      description: 'Servicios varios'
    }
  ];

  employeeForm: FormGroup;
  desabledInputs: boolean = false;

  constructor(private store: Store<AppState>, private fb: FormBuilder, public dialog: MatDialog, 
    private employeeService: EmployeeService, private route: Router) { }

  ngOnInit(): void {

    this.employee$ = this.store.select(selectEmployee);
    this.employeeForm = this.initForm();
    this.employee$.subscribe(employee => {
      if (employee.idEmployee) {
        console.log(employee);
        this.desabledInputs = true;
        this.employeeForm = this.initForm();
        this.employeeForm.controls['fristSurname'].setValue(employee.fristSurname);
        this.employeeForm.controls['secondSurname'].setValue(employee.secondSurname);
        this.employeeForm.controls['name'].setValue(employee.name);
        this.employeeForm.controls['otherNames'].setValue(employee.otherNames);
        this.employeeForm.controls['country'].setValue(employee.employmentsCountry);
        this.employeeForm.controls['registrationDate'].setValue(employee.registrationDate);
        this.employeeForm.controls['idType'].setValue(employee.typeId);
        this.employeeForm.controls['idNumber'].setValue(employee.idEmployee);
        this.employeeForm.controls['area'].setValue(employee.area.id);
      }
    });
    
  }

  initForm(): FormGroup {

    return this.fb.group({
      fristSurname: ['', Validators.compose([Validators.required, this.customValidator])],
      secondSurname: ['', Validators.compose([Validators.required, this.customValidator])],
      name: ['', Validators.compose([Validators.required, this.customValidator])],
      otherNames: ['',  this.customValidator],
      country: ['', Validators.required],
      registrationDate: [{disabled: this.desabledInputs, value: ''}, Validators.required],
      idType: ['', Validators.required],
      idNumber: ['', Validators.required],
      area: ['', Validators.required],
    });

  }

  customValidator(control: FormControl): Validator {

    const nameRegexp: RegExp = /[ñ()_+\-=\[\]{};':"\\|,.<>\/?]+/;
    if (control.value && nameRegexp.test(control.value)) {
      return { invalidName: true };
    } else {
      return {} as Validator;
    }

  }

  save() {

    if (!this.employeeForm.valid) {
      this.dialog.open(ModalMessageComponent, {
        data: {
          message: 'The form is invalid, please enter the data correctly'
        }
      });
      return;
    }

    let newInfoEmployee: Employee = {} as Employee;
    let helperDate = new Date();

    newInfoEmployee.fristSurname = this.employeeForm.controls['fristSurname'].value.toUpperCase();
    newInfoEmployee.secondSurname = this.employeeForm.controls['secondSurname'].value.toUpperCase();
    newInfoEmployee.name = this.employeeForm.controls['name'].value.toUpperCase();
    newInfoEmployee.otherNames = this.employeeForm.controls['otherNames'].value.toUpperCase();
    newInfoEmployee.employmentsCountry = this.employeeForm.controls['country'].value;
    newInfoEmployee.registrationDate = moment(this.employeeForm.controls['registrationDate'].value).toDate();
    newInfoEmployee.typeId = parseInt(this.employeeForm.controls['idType'].value);
    newInfoEmployee.idEmployee = this.employeeForm.controls['idNumber'].value;
    newInfoEmployee.area = this.areas.filter(v => v.id === parseInt(this.employeeForm.controls['area'].value))[0];
    newInfoEmployee.status = 'Activo';

    // Is update employee
    if (this.desabledInputs) {
      newInfoEmployee.editionDate = moment(helperDate).toDate();
    }
    console.log('newInfoEmployee', newInfoEmployee);

    
    helperDate.setMonth(helperDate.getMonth() - 1);
    const insertedDate = new Date(this.employeeForm.controls['registrationDate'].value);
    
    if (insertedDate < helperDate && !this.desabledInputs) {
      this.dialog.open(ModalMessageComponent, {
        data: {
          message: 'The admission date cannot be less than one month from the current date'
        }
      });
      return;
    }

    if (this.desabledInputs) {
      this.updateEmployee(newInfoEmployee);
    } else {
      this.createEmployee(newInfoEmployee);
    }

  }

  updateEmployee(employee: Employee) {
    this.employeeService.updateEmploye(employee).subscribe(res => {
      this.dialog.open(ModalMessageComponent, {
        data: {
          message: 'Employee updated successfully'
        }
      });
    },
    err => {
      this.dialog.open(ModalMessageComponent, {
        data: {
          message: err.error.message
        }
      });
    });
  }

  createEmployee(employee: Employee) {
    this.employeeService.createEmployee(employee).subscribe(res => {
      this.dialog.open(ModalMessageComponent, {
        data: {
          message: 'Employee created successfully'
        }
      });
    },
    err => {
      this.dialog.open(ModalMessageComponent, {
        data: {
          message: err.error.message
        }
      });
    });
  }

  goBack() {
    let employee: Employee = {} as Employee;
    this.store.dispatch(loadEmployeeUpdate({employee: employee}));
    this.route.navigate(['']);
  }

}

