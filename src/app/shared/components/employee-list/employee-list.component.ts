import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { Employee } from 'src/app/core/models/Employee';
import { EmployeeService } from '../../services/employee.service';
import { MatDialog } from '@angular/material/dialog';
import { ModalMessageComponent } from '../modals/modal-message/modal-message.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { ModalConfirmComponent } from '../modals/modal-confirm/modal-confirm.component';
import { Store } from '@ngrx/store';
import { loadEmployeeUpdate } from 'src/app/state/actions/employee.actions';
import { AppState } from 'src/app/state/app.state';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements  AfterViewInit {

  displayedColumns: string[] = ['name', 'otherNames', 'fristSurname', 'secondSurname',
   'typeId','idEmployee', 'employmentsCountry', 'email', 'status', 'action'];
   employeeList: MatTableDataSource<Employee>;
   resultsLength: number = 0;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort ;


  constructor(private employeeService: EmployeeService, public dialog: MatDialog, private route: Router,
    private store: Store<AppState>) {
    this.employeeList = new MatTableDataSource([] as Employee[]);
  }

  ngAfterViewInit() {
    this.loadData();
  }

  loadData() {
    this.employeeService.getEmployees().subscribe(employees => {
      this.employeeList = new MatTableDataSource(employees);
      this.resultsLength = employees.length;
      this.employeeList.paginator = this.paginator;
      this.employeeList.sort = this.sort;
    });
  }

  applyFilter(event: Event) {

    const filterValue = (event.target as HTMLInputElement).value;
    this.employeeList.filter = filterValue.trim().toLowerCase();

    if (this.employeeList.paginator) {
      this.employeeList.paginator.firstPage();
    }

  }

  clickDeleteEmployee(employee: Employee) {

    let confirm = this.dialog.open(ModalConfirmComponent, {
      data: {
        message: 'Are you sure you want to delete the employee? ',
      },
    });
    confirm.afterClosed().subscribe(result => {
      console.log(result);
      if (result) {
        this.deleteEmployee(employee);
      }
    });
    
  }

  deleteEmployee(employee: Employee) {
    console.log('Entre');
    this.employeeService.deleteEmployee(employee).subscribe(res => {
      this.dialog.open(ModalMessageComponent, {
        data: {
          message: 'The employee has been successfully removed',
        },
      });
      this.loadData();
    });
  }

  editEmployee(employee: Employee) {
    this.store.dispatch(loadEmployeeUpdate({employee: employee}));
    this.route.navigate(['detail-employee']);
  }

  newEmployee() {
    this.route.navigate(['detail-employee']);
  }

}
