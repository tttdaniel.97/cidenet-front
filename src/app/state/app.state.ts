import { ActionReducerMap } from "@ngrx/store";
import { EmployeeState } from "../core/models/EmployeeState";
import {  employeeReducer } from "./reducers/employee.reducers";

export interface AppState {
  employees: EmployeeState;
}

export const ROOT_REDUCERS:ActionReducerMap<AppState> = {
    employees: employeeReducer,
};