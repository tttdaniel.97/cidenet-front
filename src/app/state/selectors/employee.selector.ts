import { createSelector } from '@ngrx/store';
import { EmployeeState } from 'src/app/core/models/EmployeeState';
import { AppState } from '../app.state';
 
export const selectEmployeesFeature = (state: AppState) => state.employees;
 
export const selectEmployee = createSelector(
    selectEmployeesFeature,
    (state: EmployeeState) => state.employee
);