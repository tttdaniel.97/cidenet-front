import { createReducer, on } from '@ngrx/store';
import { Employee } from 'src/app/core/models/Employee';
import { EmployeeState } from 'src/app/core/models/EmployeeState';
import { loadEmployeeUpdate } from '../actions/employee.actions';

export const initialState: EmployeeState = {
    employee: {} as Employee
}

export const employeeReducer = createReducer(
  initialState,
  on(loadEmployeeUpdate, (state, { employee }) => {
    return { ...state, employee: {...employee} }
  })
);
