import { createAction, props } from '@ngrx/store';
import { Employee } from 'src/app/core/models/Employee';
 
export const loadEmployeeUpdate = createAction(
    '[Employee List] Loaded employee to update',
    props<{ employee: Employee }>()
);

